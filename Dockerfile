FROM docker.io/ansible/ansible-runner:latest

ENV BASE_PROJECT_BADGE ""


COPY config/ /project/config/
COPY badge/ /project/badge/
RUN pip3 install --upgrade pip \
	    && pip3 install --no-cache ansible-lint \
	    && pip3 install --no-cache requests \
	    && chmod +x /project/config/start.sh \
	    && chmod +x /project/config/function.py

WORKDIR /project/playbook
ENTRYPOINT ["/project/config/start.sh"]
